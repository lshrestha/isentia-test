# Task 1. Floats.
- [ ] Please explain why this code `console.log(0.1 + 0.2 == 0.3);` outputs `false` in console?
- [ ] Please suggest fix to make this expression working as expected.
- It because the numbers are floating point numbers and the precision level is high.
- The result of the addition is 0.30000000000000004 which is not equal to 0.3 hence the - result.
- We can fix this by either using toFixed() or toPrecision() function on the result.

# Fix

```javascript
console.log((0.1 + 0.2).toFixed(2) == 0.3)
```

# Task 2. Sum function.
- [ ] Write a `sum` function which will work properly when invoked using either syntax below:

```javascript
console.log(sum(2,3)); // Outputs 5
console.log(sum(2)(3)); // Outputs 5
```

```javascript
function sum(a) {
  if (arguments.length == 2) {
    return arguments[0] + arguments[1];
  } else {
    return function(b) { return a + b; };
  }
}
```

- [ ] Make the function working for any number of arguments.

```javascript
var sum = function() {
  var total = 0;
  for (var i = 0; i < arguments.length; i++) {
    if (!isNaN(arguments[i])) {
      total += arguments[i];
    }
  }

  function closureSum() {
    if (arguments.length > 0) {
      for (var i = 0; i < arguments.length; i++) {
        if (!isNaN(arguments[i])) {
          total += arguments[i];
        }
      }
    }
    return closureSum;
  }

  closureSum.toString = function() {
    return total;
  };
  closureSum.valueOf = function() {
    return total;
  };
  return closureSum;
};
console.log(sum(2)(3).toString());
console.log(sum1(2, 2, 323)(112, 0, 33)(2).valueOf());
```
- This function can be called as shown above with many arguments and parenthesis.
The value is returned through toString()/valueOf() which is usually invoked through
console.log() but FireFox/Chrome may not do so. Hence, to get the correct value instead
of the object, we have to call the toString()/valueOf() function explicitly.
# Task 3. Loops.
Consider you have code snippet like this:

```javascript
for (var i = 0; i < 5; i++) {
  var btn = document.createElement('button');
  btn.appendChild(document.createTextNode('Button ' + i));
  btn.addEventListener('click', function(){ console.log(i); });
  document.body.appendChild(btn);
}
```

- [ ] What will be the output when user clicks on `Button 1` and why?

- The output will be the last value of the variable i after the loop has completed i.e. 5 because
when the eventlistener is invoked, it is referencing to the value of i which is already updated to 5.
- [ ] Please, suggest a fix to get the expected behavior.
- One way to fix it is by using closure, which stores the value of i at the time of its creation.

```javascript
// by creating closure
for (var i = 0; i < 5; i++) {
  var btn = document.createElement('button');
  btn.appendChild(document.createTextNode('Button ' + i));
  btn.addEventListener('click', (function(i) {
    return function() {
      console.log(i);
    };
  })(i));
  document.body.appendChild(btn);
}
```

-another way is by using data-attrib of HTML which is useful if we have to do DOM manipulations
//using data attrib
```javascript
for (var i = 0; i < 5; i++) {
  var btn = document.createElement('button');
  btn.setAttribute("number", i);
  btn.appendChild(document.createTextNode('Button ' + i));
  document.body.appendChild(btn);
}
document.body.addEventListener('click', function(e) {
  if (e.target.type === "submit" || e.target.type === "reset") {
    console.log(e.target.getAttribute("number"));
  }
});
```


# Task 4. Cache buster function.
Write a JS function `cachebuster`.
- [ ] The function should return new unique value (string or integer) every 5 minutes. See example below.
- [ ] Make this function as short as it is possible.

```javascript
function cachebuster() {
 // your code goes here
}

console.log(cachebuster()); // Called immediately, outputs string or integer, e.g. "abcd" or 1234
console.log(cachebuster()); // Called after 1 minutes, outputs the same string: "abcd" or 1234
console.log(cachebuster()); // Called after 2 minutes, still outputs: "abcd" or 1234

// ...

console.log(cachebuster()); // Called after 5 minutes, outputs new unique value: e.g. "abce" or 1235
console.log(cachebuster()); // Called after 6 minutes, outputs previous value: "abce" or 1235

// ...

console.log(cachebuster()); // Called after 10 minutes, outputs another one new unique value, like "abcf" or 1236
```


- CacheBuster function


```javascript
var cachebuster = function(timeout) {
  var dateTime = new Date();
  var refreshTime = timeout || 5;
  var currentTime = (dateTime.getHours() * 60) + dateTime.getMinutes();
  var defaultCache = {
    secret: "1234",
    creationTime: (currentTime - timeout - 1) //timeout substracted otherwise the default cache won't be updated
  };

  if (typeof(Storage) !== "undefined") {
    var currentCache = JSON.parse(localStorage.getItem("cache")) || defaultCache;
    var timeDiff = Math.abs(currentTime - currentCache.creationTime);
    if (timeDiff > refreshTime) {
      var date = new Date();
      var secret = crypto.getRandomValues(new Uint32Array(1)).toString().substring(3, 7);
      var creationTime = (parseInt(date.getHours(), 10) * 60) + parseInt(date.getMinutes(), 10);
      var storageItem = {
        secret: secret,
        creationTime: creationTime
      };
      localStorage.setItem("cache", JSON.stringify(storageItem));
      currentCache = storageItem;
    }
  } else {
    //fallback either using cookies or third-party library such as PersistJS
  }
  return currentCache.secret;
};

cachebuster();

```
