'use strict';

(function() {
  // Photos Controller Spec
  describe('Photos Controller Tests', function() {
    // Initialize global variables
    var PhotosController,
      scope,
      $httpBackend,
      $stateParams,
      $location,
      Authentication,
      Photos,
      url,
      errorResponse,
      mockPhoto,
      mockPhotoObj;

    // The $resource service augments the response object with methods for updating and deleting the resource.
    // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
    // the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
    // When the toEqualData matcher compares two objects, it takes only object properties into
    // account and ignores methods.
    beforeEach(function() {
      jasmine.addMatchers({
        toEqualData: function(util, customEqualityTesters) {
          return {
            compare: function(actual, expected) {
              return {
                pass: angular.equals(actual, expected)
              };
            }
          };
        }
      });
    });
    afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
      $httpBackend.resetExpectations();
    });


    // Then we can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, _Authentication_, _Photos_) {
      // Set a new global scope
      scope = $rootScope.$new();

      // Point global variables to injected services
      $stateParams = _$stateParams_;
      $httpBackend = _$httpBackend_;
      $location = _$location_;
      Authentication = _Authentication_;
      Photos = _Photos_;

      // create mock photo
      mockPhoto = {
        'title': 'Uploads from everyone',
        'link': 'https://www.flickr.com/photos/',
        'description': '',
        'modified': '2016-01-11T02:54:43Z',
        'generator': 'https://www.flickr.com/',
        'items': [{
          'title': '20160106 NSR/RNET 2010, Amsterdam CS',
          'link': 'https://www.flickr.com/photos/berthollander/23911882460/',
          'media': {
            'm': 'https://farm2.staticflickr.com/1651/23911882460_ca4122dd85_m.jpg'
          },
          'date_taken': '2016-01-06T09:45:35-08:00',
          'description': ' desc ',
          'published': '2016-01-06T09:45:07Z',
          'author': 'nobody@flickr.com (Bert Hollander)',
          'author_id': '57787036@N06',
          'tags': 'flirt'
        }, {
          'title': '20160106 NSR 186 014 & 239, Amsterdam CS',
          'link': 'https://www.flickr.com/photos/berthollander/24186638126/',
          'media': {
            'm': 'https://farm2.staticflickr.com/1660/24186638126_86db3e13ca_m.jpg'
          },
          'date_taken': '2016-01-06T09:58:18-08:00',
          'description': 'desc1',
          'published': '2016-01-06T16:18:07Z',
          'author': 'nobody@flickr.com (Bert Hollander)',
          'author_id': '57787036@N06',
          'tags': ' geel wit'
        }]
      };
      mockPhotoObj = [{
        'thumb': 'https://farm2.staticflickr.com/1651/23911882460_ca4122dd85_m.jpg',
        'img': 'https://farm2.staticflickr.com/1651/23911882460_ca4122dd85_b.jpg',
        'description': '20160106 NSR/RNET 2010, Amsterdam CS',
        'tags': 'flirt'
      }, {
        'thumb': 'https://farm2.staticflickr.com/1660/24186638126_86db3e13ca_m.jpg',
        'img': 'https://farm2.staticflickr.com/1660/24186638126_86db3e13ca_b.jpg',
        'description': '20160106 NSR 186 014 & 239, Amsterdam CS',
        'tags': ' geel wit'
      }];
      url = 'https://api.flickr.com/services/feeds/photos_public.gne?&format=json&jsoncallback=JSON_CALLBACK';

      errorResponse = {
        "status": 404,
        "code": 99,
        "message": "Insufficient permissions. Method requires read privileges; none granted."
      };
      // $httpBackend.whenJSONP(url).respond(400, {
      //   error: "error occured"
      // });


      // Mock logged in user
      Authentication.user = {
        roles: ['user']
      };

      // Initialize the Photos controller.
      PhotosController = $controller('PhotosController', {
        $scope: scope
      });
    }));

    it('$scope.search() should create an array with at least one photo object fetched from XHR', inject(function(Photos) {
      $httpBackend.whenJSONP().respond(mockPhoto);
      // Run controller functionality
      scope.search();
      $httpBackend.flush();

      // Test scope value
      expect(scope.photos).toEqualData(mockPhotoObj);
    }));

    it('$scope.search() should show an message incase of error', inject(function(Photos) {
      $httpBackend.whenJSONP(url).respond(400, {
        error: "error occured"
      });
      scope.search();
      $httpBackend.flush();
      expect(scope.feedSearchFailure).toBeTruthy();

    }));

    it('$scope.createImgObj() should create an array of images', inject(function(Photos) {
      scope.createImgObj(mockPhoto.items);
      expect(scope.photos).toEqualData(mockPhotoObj);
    }));
  });
}());
