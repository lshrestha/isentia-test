'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Article Schema
 */
var PhotoSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  caption: {
    type: String,
    default: '',
    trim: true,
    required: 'Caption cannot be blank'
  },
  url: {
    type: String,
    default: '',
    trim: true
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Photo', PhotoSchema);
