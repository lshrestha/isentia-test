'use strict';

// Photos controller
angular.module('photos').controller('PhotosController', ['$scope', '$stateParams', '$location', '$http', 'Authentication', 'Photos',
  function($scope, $stateParams, $location, $http, Authentication, Photos) {
    $scope.authentication = Authentication;

    $scope.tags = '';
    $scope.photos = [];
    $scope.feedSearchFailure = false;
    $scope.loading = false;
    $scope.imageSearched = false;

    $scope.search = function(tags) {
      $scope.imageSearched = ($scope.tags) ? true : false;
      $scope.feedSearchFailure = false;
      $scope.searchURL = 'https://api.flickr.com/services/feeds/photos_public.gne?';
      var config = {
        params: {
          format: 'json',
          jsoncallback: 'JSON_CALLBACK'
        }
      };
      if ($scope.tags !== '') {
        config.params.tags = $scope.tags;
      }
      $http.jsonp($scope.searchURL, config).then(function(response) {
        $scope.createImgObj(response.data.items);
      }).catch(function(data, status, headers, config) {
        $scope.feedSearchFailure = true;
      });
    };

    $scope.createImgObj = function(photos) {
      var tempObj = [];
      var imgObj = {};
      angular.forEach(photos, function(photo) {
        imgObj = {
          thumb: photo.media.m,
          img: photo.media.m.replace('_m.jpg', '_b.jpg'),
          description: photo.title,
          tags: photo.tags
        };
        tempObj.push(imgObj);
      });
      $scope.photos = tempObj;
    };

  }
]);
