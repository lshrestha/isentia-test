'use strict';

//Photos service used for communicating with the photos REST endpoints
angular.module('photos').factory('Photos', ['$resource',
  function ($resource) {
    return $resource('api/photos/:photoId', {
      photoId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
