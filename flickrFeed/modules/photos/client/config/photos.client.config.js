'use strict';

// Configuring the Photos module
angular.module('photos').run(['Menus',
  function (Menus) {
    // Add the photos dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Photos',
      state: 'photos',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'photos', {
      title: 'List Photos',
      state: 'photos.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'photos', {
      title: 'Create Photos',
      state: 'photos.create',
      roles: ['user']
    });
  }
]);
